require 'test_helper'

class OrderItemHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order_item_history = order_item_histories(:one)
  end

  test "should get index" do
    get order_item_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_order_item_history_url
    assert_response :success
  end

  test "should create order_item_history" do
    assert_difference('OrderItemHistory.count') do
      post order_item_histories_url, params: { order_item_history: { item_id: @order_item_history.item_id, order_history_id: @order_item_history.order_history_id, units: @order_item_history.units } }
    end

    assert_redirected_to order_item_history_url(OrderItemHistory.last)
  end

  test "should show order_item_history" do
    get order_item_history_url(@order_item_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_order_item_history_url(@order_item_history)
    assert_response :success
  end

  test "should update order_item_history" do
    patch order_item_history_url(@order_item_history), params: { order_item_history: { item_id: @order_item_history.item_id, order_history_id: @order_item_history.order_history_id, units: @order_item_history.units } }
    assert_redirected_to order_item_history_url(@order_item_history)
  end

  test "should destroy order_item_history" do
    assert_difference('OrderItemHistory.count', -1) do
      delete order_item_history_url(@order_item_history)
    end

    assert_redirected_to order_item_histories_url
  end
end
