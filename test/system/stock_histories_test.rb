require "application_system_test_case"

class StockHistoriesTest < ApplicationSystemTestCase
  setup do
    @stock_history = stock_histories(:one)
  end

  test "visiting the index" do
    visit stock_histories_url
    assert_selector "h1", text: "Stock Histories"
  end

  test "creating a Stock history" do
    visit stock_histories_url
    click_on "New Stock History"

    fill_in "Buy price", with: @stock_history.buy_price
    fill_in "Item", with: @stock_history.item_id
    fill_in "Units", with: @stock_history.units
    click_on "Create Stock history"

    assert_text "Stock history was successfully created"
    click_on "Back"
  end

  test "updating a Stock history" do
    visit stock_histories_url
    click_on "Edit", match: :first

    fill_in "Buy price", with: @stock_history.buy_price
    fill_in "Item", with: @stock_history.item_id
    fill_in "Units", with: @stock_history.units
    click_on "Update Stock history"

    assert_text "Stock history was successfully updated"
    click_on "Back"
  end

  test "destroying a Stock history" do
    visit stock_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Stock history was successfully destroyed"
  end
end
