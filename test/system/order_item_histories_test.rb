require "application_system_test_case"

class OrderItemHistoriesTest < ApplicationSystemTestCase
  setup do
    @order_item_history = order_item_histories(:one)
  end

  test "visiting the index" do
    visit order_item_histories_url
    assert_selector "h1", text: "Order Item Histories"
  end

  test "creating a Order item history" do
    visit order_item_histories_url
    click_on "New Order Item History"

    fill_in "Item", with: @order_item_history.item_id
    fill_in "Order history", with: @order_item_history.order_history_id
    fill_in "Units", with: @order_item_history.units
    click_on "Create Order item history"

    assert_text "Order item history was successfully created"
    click_on "Back"
  end

  test "updating a Order item history" do
    visit order_item_histories_url
    click_on "Edit", match: :first

    fill_in "Item", with: @order_item_history.item_id
    fill_in "Order history", with: @order_item_history.order_history_id
    fill_in "Units", with: @order_item_history.units
    click_on "Update Order item history"

    assert_text "Order item history was successfully updated"
    click_on "Back"
  end

  test "destroying a Order item history" do
    visit order_item_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Order item history was successfully destroyed"
  end
end
