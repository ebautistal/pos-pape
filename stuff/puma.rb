#!/usr/bin/env puma

directory '/home/deploy/pos-pape/current'
rackup "/home/deploy/pos-pape/current/config.ru"
environment 'production'

tag ''

pidfile "/home/deploy/pos-pape/shared/tmp/pids/puma.pid"
state_path "/home/deploy/pos-pape/shared/tmp/pids/puma.state"
stdout_redirect '/home/deploy/pos-pape/shared/log/puma_access.log', '/home/deploy/pos-pape/shared/log/puma_error.log', true


threads 0,16



bind 'unix:///home/deploy/pos-pape/shared/tmp/sockets/puma.sock'

workers 0




restart_command 'bundle exec puma'


prune_bundler


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = ""
end


