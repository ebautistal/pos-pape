Rails.application.routes.draw do

  resources :item_types
  devise_for :users
  root to: "orders#new"
  
  get '/orders/:id/submit', to: 'orders#submit'
  get '/orders/:id/resume', to: 'orders#resume'
  post '/orders/:id/complete', to: 'orders#complete'
  #get '/orders/:id/print_ticket', to: 'orders#print_ticket'
  get '/orders/print_info_pape', to: 'orders#print_info_pape'
  get '/orders/:id/print_order', to: 'orders#print_order'

  resources :order_item_histories
  resources :order_histories
  
  resources :orders do
    resources :order_items
  end
  
  resources :order_statuses
  resources :item_prices
  resources :stock_histories
  resources :stocks

  resources :items do
    get 'search', on: :collection
  end

  resources :brands

  get '/test_ticket', to: 'ticket_test#ticket'
  #get '/report_order_item', to: 'report_order_item#index'
  get '/reports/items', to: 'reports#items'
  get '/reports/totals_day', to: 'reports#totals_day'


  #get 'search_brands', to: 'brands#search'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
