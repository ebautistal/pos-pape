class ReportsController < ApplicationController

  def items
    
    sql = "select oih.item_id, oih.item_name, sum(quantity) as sum_quantity 
     from order_item_histories oih
     where created_at >= $1 and created_at<=$2
     group by oih.item_id, oih.item_name order by 3 desc"

    case params[:range]
    when "day"
      @data = ActiveRecord::Base.connection.exec_query(sql, "SQL", [[nil,Time.now.beginning_of_day], [nil,Time.now.end_of_day]])
    when "week"
      @data = ActiveRecord::Base.connection.exec_query(sql, "SQL", [[nil,Time.now.beginning_of_week], [nil,Time.now.end_of_day]])
    when "month"
      @data = ActiveRecord::Base.connection.exec_query(sql, "SQL", [[nil,Time.now.beginning_of_month], [nil,Time.now.end_of_day]])
    when "year"
      @data = ActiveRecord::Base.connection.exec_query(sql, "SQL", [[nil,Time.now.beginning_of_year], [nil,Time.now.end_of_day]])
    else
      @data = ActiveRecord::Base.connection.exec_query(sql, "SQL", [[nil,Time.now.beginning_of_day], [nil,Time.now.end_of_day]])
    end

  end

  def totals_day
    closed_status = OrderStatus.find_by name: 'CLOSED'

    sql_select = "date_trunc('day',created_at) as created_at, 
      sum(count_items) as count_items, 
      sum(subtotal_amount) as subtotal_amount, 
      sum(total_discount) as total_discount, 
      sum(total_amount) as total_amount"

    sql_where = 'order_status_id = ? and created_at BETWEEN ? AND ?'

    group_by = "date_trunc('day',created_at)"
    
    case params[:range]
    when "day"
      @data = OrderHistory.select(sql_select).where(sql_where, closed_status.id, Time.now.beginning_of_day, Time.now.end_of_day).group(group_by).order("1 desc")
    when "week"
      @data = OrderHistory.select(sql_select).where(sql_where, closed_status.id, Time.now.beginning_of_week, Time.now.end_of_day).group(group_by).order("1 desc")
    when "month"
      @data = OrderHistory.select(sql_select).where(sql_where, closed_status.id, Time.now.beginning_of_month, Time.now.end_of_day).group(group_by).order("1 desc")
    when "year"
      @data = OrderHistory.select(sql_select).where(sql_where, closed_status.id, Time.now.beginning_of_year, Time.now.end_of_day).group(group_by).order("1 desc")
    else
      @data = OrderHistory.select(sql_select).where(sql_where, closed_status.id, Time.now.beginning_of_day, Time.now.end_of_day).group(group_by).order("1 desc")
    end

  end

end
