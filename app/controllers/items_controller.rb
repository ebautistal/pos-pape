class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  ITEMS_PER_PAGE = 50

  # GET /items
  # GET /items.json
  def index
    @like_search = params.fetch(:like_search, "")
    @page = params.fetch(:page, 1).to_i

    words = @like_search.split(" ")
    sql = ""
    
    words.each_with_index do |word, index|
      if index > 0 
        sql += " and "
      end
      sql += "(lower(name) like lower('%#{word}%') or lower(description) like lower('%#{word}%') or lower(code) like lower('%#{word}%'))"
    end    

    @total_items = Item.where(sql).size
    @items = Item.where(sql).offset((@page-1)*ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE).includes(:item_price, :brand, :stock)
    @total_pages = 0

    return if @total_items==0

    @mod_items = @total_items%ITEMS_PER_PAGE
    if @mod_items>0
      @total_pages = @total_items/ITEMS_PER_PAGE + 1
    else
      @total_pages = @total_items/ITEMS_PER_PAGE
    end
    
  end

  # GET /items/1
  # GET /items/1.json
  def show
  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new (item_params)

    if @item.save
      save_references_item
      redirect_to @item, notice: 'Articulo creado exitosamente'
    else
      render :new
    end
    
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    if @item.update(item_params)
      save_references_item
      redirect_to @item, notice: 'Articulo actualizado exitosamente'
    else
      render :edit
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    if OrderItem.where(item_id: @item.id).size() >0 then
      redirect_to items_url, alert: 'Articulo en uso en una Orden pendiente, no se puede eliminar'
    else
      @item.destroy
      redirect_to items_url, notice: 'Item was successfully destroyed.'
    end
  end

  def search
    sql_where = "(item_prices.price > 0 or item_prices.variable_price)"
    words = params.fetch(:like_search, "").split(" ")
    code = params.fetch(:code, "")

    replacements = { "'" => "''"}

    words.each_with_index do |word, index|
      word = word.gsub(Regexp.union(replacements.keys), replacements)
      sql_where += " and (lower(items.name) like lower('%#{word}%') or lower(items.description) like lower('%#{word}%') or lower(code) like lower('%#{word}%') )"
    end

    if code.present? then
      code = code.gsub(Regexp.union(replacements.keys), replacements)
      sql_where += " and lower(items.code) = lower('#{code}')"
    end

    @items = Item.joins(:item_price).where(sql_where).includes(:item_price, :brand).limit(50)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def item_params
      params.require(:item).permit(:code, :brand_id, :name, :description)
    end

    def save_references_item

      if params[:item_price].present?
        variable_price = params[:item_price][:variable_price]
        price = params[:item_price][:price]
        price = price.to_f.round(2) if price.present?

        if @item.item_price.present?
          @item.item_price.price = price
          @item.item_price.variable_price = variable_price
          @item.item_price.save
        else
          ItemPrice.create! item_id: @item.id, price: price, variable_price: variable_price
        end

      end

      if params[:stock].present?
        units = params[:stock][:units]
        item_shareable = params[:stock][:item_shareable]
        buy_price = params[:stock][:buy_price]
        shared_stock = params[:stock][:shared_stock]
        shared_stock_item_id = params[:stock][:shared_stock_item_id]
        shared_stock_consume_units = params[:stock][:shared_stock_consume_units]

        units = units.to_f.round(2) if units.present?
        buy_price = buy_price.to_f.round(2) if buy_price.present?
        shared_stock_consume_units = shared_stock_consume_units.to_f.round(2) if shared_stock_consume_units.present?

        if @item.stock.present?
          @item.stock.units = units
          @item.stock.item_shareable = item_shareable
          @item.stock.buy_price = buy_price
          @item.stock.shared_stock = shared_stock
          @item.stock.shared_stock_item_id = shared_stock_item_id
          @item.stock.shared_stock_consume_units = shared_stock_consume_units
          @item.stock.save
        else
          Stock.create! item_id: @item.id, units: units, item_shareable: item_shareable, buy_price: buy_price, shared_stock: shared_stock, shared_stock_item_id: shared_stock_item_id, shared_stock_consume_units: shared_stock_consume_units
        end

      end

    end
end
