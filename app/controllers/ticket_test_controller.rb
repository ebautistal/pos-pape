class TicketTestController < ApplicationController
  ESC = "\x1B"
  LF  = "\x0A"
  GS  = "\x1D"
  CHARS_LINE_ESCPOS = 40

  before_action :init_escpos, only: [:ticket]

  def ticket
  	print_ticket
  end

  private

    def init_escpos
      @cmds = "".force_encoding("ASCII-8BIT")
      write_escpos ESC + "@"
    end

    def write_escpos (data)
      @cmds << data.force_encoding("ASCII-8BIT")
    end
    #alias :<<

    def print_logo_escpos
      image = ::MiniMagick::Image.open("#{Rails.root}/public/logo_sodimac.jpg")
      print_image "G2", image
    end

    def print_qr_escpos
      image = ::MiniMagick::Image.open("#{Rails.root}/public/qr_sodimac.jpg")
      print_image "G3", image
    end

    def print_image (key, image)
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr

      total_pixels = image.width * image.height
      bytes_next_m = (total_pixels)/8 + 11
      pL = bytes_next_m%256
      pH = bytes_next_m/256
      xL = image.width%256
      xH = image.width/256
      yL = image.height%256
      yH = image.height/256

      #Set graphics data: [Function 67] Define the NV graphics data (raster format)
      #128(=8*16) dots wide and 120 dots tall with respect to key code "G1"
      #        GS   ( L    pL       pH       m        fn       a      kc1/Kc2  b       xL       xH       yL       yH       c
      write_escpos GS + "(L" + pL.chr + pH.chr + 48.chr + 67.chr + 48.chr + key + 1.chr + xL.chr + xH.chr + yL.chr + yH.chr + 49.chr

      #Data image
      write_escpos data_image image

      #Set graphics data: [Function 69] Print the specified NV graphics data.
      #Prints data that corresponds to key code "G1" at 1x1 size.
      #        GS   ( L    pL      pH      m        fn     kc1/Kc2  x       y
      write_escpos GS + "(L" + 6.chr + 0.chr + 48.chr + 69.chr + key + 1.chr + 1.chr
    end

    def print_stamp_escpos
      #Set line spacing: For the TM-T20, 1.13 mm (18/406 inches)
      write_escpos ESC + "3" + 18.chr
      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Select character size: Normal size
      write_escpos GS + "!" + 0.chr
      write_escpos "RFC: CSD161207R2A" + LF
      write_escpos "COMERCIALIZADORA SDMHC SA DE CV" + LF
      write_escpos "AV. ADOLFO LOPEZ MATEOS 201" + LF
      write_escpos "SANTA CRUZ ACATLAN, NAUCALPAN" + LF
      write_escpos "ESTADO DE MEXICO CP.53150" + LF
      write_escpos " " + LF
      write_escpos "GIRO: MEJORAMIENTO DEL HOGAR" + LF
      write_escpos " " + LF
      write_escpos "TIENDA 1030" + LF
      write_escpos "SODIMAC HOMECENTER AROLEDAS" + LF
      write_escpos "AV. GUSTAVO BAZ 4001" + LF
      write_escpos "CENTRO INDUSTRIAL TLALNEPANTLA" + LF
      write_escpos " TLALNEPANTLA  ESTADO DE MEXICO CP.54030" + LF
      write_escpos " " + LF
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      write_escpos "TICKET DE VENTA" + LF
      write_escpos " " + LF
      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      write_escpos "NO. TRANSACCION 7572" + LF
      write_escpos "CAJA 0001" + LF
      write_escpos "FECHA 14/03/2020    HORA 11:07:04" + LF
      write_escpos "CAJERO 0000000011030 Selfcheckout01" + LF
      write_escpos " " + LF
      write_escpos " " + LF

    end

    def print_order_escpos
      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Details text data and print and line feed
      write_escpos "SKU            CANTIDAD  UNIDAD     PRECIO" + LF
      write_escpos "PRODUCTO                             TOTAL" + LF
      write_escpos "------------------------------------------" + LF
      write_escpos "29563            1  x     CU      1,269.70" + LF
      write_escpos "SOBREMURO RBBOQUILLA              1,269.70" + LF
      write_escpos "69783            1  x     CU        823.00" + LF
      write_escpos "ARQUETA                             823.00" + LF
      write_escpos "13480            1  x     CU        240.00" + LF
      write_escpos "SPX MANGUERA FLEX RIEGO             240.00" + LF
      write_escpos "82933            8  x     CU        148.50" + LF
      write_escpos "BOQUILLAS RB RIEGO                1,188.00" + LF
      write_escpos "25353           20  x     CU         49.00" + LF
      write_escpos "TUBO PVC 3/4 3M                     980.00" + LF
      write_escpos " " + LF
      write_escpos "TOTAL ARTICULOS                         31" + LF
      write_escpos "                      SUBTOTAL    3,879.91" + LF
      write_escpos "                           IVA      620.79" + LF
      write_escpos "                       TOTAL $    4,500.70" + LF
      write_escpos " " + LF
      write_escpos "SON CUATRO MIL QUINIENTOS PESOS 70/100 M.N" + LF
      write_escpos ". " + LF
      write_escpos " " + LF
      write_escpos "FORMAS DE PAGO " + LF
      write_escpos "------------------------------------------" + LF
      write_escpos "EFECTIVO                          4,500.70" + LF
      write_escpos "CAMBIO                                0.00" + LF
      write_escpos " " + LF
      write_escpos " " + LF
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      write_escpos "CALIFIQUE SU EXPERIENCIA DE COMPRA EN" + LF
      write_escpos "www.sodimac.com.mx/tuopinion" + LF
      write_escpos " " + LF
      write_escpos " " + LF

      print_qr_escpos
      
      write_escpos " " + LF
      write_escpos " " + LF
      write_escpos "INGRESANDO EL CODIGO" + LF
      write_escpos " " + LF
      write_escpos "E2A-9IBH" + LF
      write_escpos " " + LF
      write_escpos " " + LF
      write_escpos "GUARDE ESTE TICKET DE VENTA" + LF
      write_escpos "Y PRESENTELO EN CASO DE" + LF
      write_escpos "RECLAMO O DEVOLUCION DE UN PRODUCTO" + LF
      write_escpos " " + LF
      write_escpos "GRACIAS POR SU COMPRA" + LF

    end

    def print_barcode_escpos
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      #write_escpos "<< Orden Id : #{@order.id} >>"
      #Print and feed paper: Paper feeding amount = 4.94 mm (35/180 inches)
      write_escpos ESC + "J" + 35.chr
      #Set barcode height: in case TM-T20, 6.25 mm (50/203 inches)
      write_escpos GS + "h" + 100.chr
      #Select print position of HRI characters: Print position, below the barcode
      write_escpos GS + "H" + 2.chr
      #Select font for HRI characters: Font B
      write_escpos GS + "f" + 1.chr
      #Print barcode: (A) format, barcode system = CODE39

      write_escpos GS + "k" + 4.chr + "*9872334*" + 0.chr
      write_escpos " " + LF
    end

    def cut_escpos
      #Operating the drawer
      #Generate pulse: Drawer kick-out connector pin 2, 2 x 2 ms on, 20 x 2 ms off
      write_escpos ESC + "p" + 0.chr + 2.chr + 20.chr

      #Select cut mode and cut paper: [Function B] Feed paper to (cutting position + 0 mm) and executes a partial cut (one point left uncut).
      write_escpos GS + "V" + 66.chr + 0.chr
    end

    def send_print_escpos
      #file_to_write = '/mnt/d/edgar/rails/pos-pape/test_print.txt'
      file_to_write = '/dev/usb/lp0'

      File.open(file_to_write,'w') do |f| 
        f << @cmds.force_encoding("UTF-8")
      end
    end

    def print_ticket

      print_logo_escpos
      print_stamp_escpos
      print_barcode_escpos
      print_order_escpos
      cut_escpos
      send_print_escpos

    end

    def data_image (mini_magic_image)
      image = mini_magic_image

      bits = []
      mask = 0x80
      i = 0
      temp = 0

      pixels = image.get_pixels

      0.upto(image.height - 1) do |y|
        0.upto(image.width - 1) do |x|
          pxR, pxG, pxB = pixels[y][x][0], pixels[y][x][1], pixels[y][x][2]
          pxValue = (pxR+pxG+pxB)/3
          value = pxValue >= 128 ? 255 : 0
          value = (value << 8) | value
          temp |= mask if value == 0
          mask = mask >> 1
          i = i + 1
          if i == 8
            bits << temp
            mask = 0x80
            i = 0
            temp = 0
          end
        end
      end

      bits.pack("C*")
    end


end
