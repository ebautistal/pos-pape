class OrdersController < ApplicationController
  ESC = "\x1B"
  LF  = "\x0A"
  GS  = "\x1D"
  CHARS_LINE_ESCPOS = 42
  OUTPUT_FILE_ESCPOS = '/dev/usb/lp0'
#   OUTPUT_FILE_ESCPOS = 'test_print.txt'

  before_action :set_order, only: [:show, :edit, :update, :destroy, :submit, :resume, :complete]
  before_action :set_order_hist, only: [:print_order]
  before_action :init_escpos, only: [:complete, :print_info_pape, :print_order]

  # GET /orders
  # GET /orders.json
  def index
    #open_status = OrderStatus.find_by name: 'OPEN'
    #@orders = Order.where("order_status_id=?",open_status.id).order(updated_at: :desc).includes(:order_status)
    @orders = Order.all.order(updated_at: :desc).includes(:order_status)
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
    @url_form = '/orders'
    @method_form = 'post'
  end

  # GET /orders/1/edit
  def edit
    @url_form = "/orders/#{@order.id}"
    @method_form = 'patch'
  end

  # POST /orders
  # POST /orders.json
  def create

    @order = Order.create!(order_params)
    
    save_order_items

    if "submit_order".eql?(params[:after_action])
      redirect_to "/orders/#{@order.id}/submit"
    else
      redirect_to @order, notice: 'Orden guardada exitosamente'
    end

  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update

    @order.update(order_params)

    #delete current order_items
    OrderItem.where("order_id=?", @order.id).delete_all

    save_order_items

    if "submit_order".eql?(params[:after_action])
      redirect_to "/orders/#{@order.id}/submit"
    else
      redirect_to @order, notice: 'Orden guardada exitosamente'
    end
    
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    canceled_status = OrderStatus.find_by name: 'CANCELED'
    @order.order_status_id = canceled_status.id
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /orders/1/submit
  def submit
  end

  # POST /orders/1/complete
  def complete
    closed_status = OrderStatus.find_by name: 'CLOSED'
    @order.order_status_id = closed_status.id
    @order.paid = params[:order][:paid].to_f.round(2)
    @order.change = (@order.paid - @order.total_amount).round(2)

    @order.save
    @order.destroy
  end

  # POST /orders/1/print_order
  def print_order
    print_ticket_hist
  end

  # POST /orders/print_info_pape
  def print_info_pape

    #if File.file?(OUTPUT_FILE_ESCPOS)
      print_logo_escpos
      print_stamp_escpos
      cut_escpos
      send_print_escpos
    #else
    #  puts 'No printer found'
    #end

  end

  # GET /orders/1/resume
  def resume
    puts params
    @count_items = 0
    @subtotal_amount = 0
    @discount_amount = 0
    @tax_amount = 0
    @total_amount = 0

    @order_items.each do |order_item|
      #TO-DO SUMS
      @count_items += order_item.quantity

      tmp_order_item_price = order_item.item.item_price
      tmp_price_per_unit = tmp_order_item_price.price

      if tmp_order_item_price.variable_price
        tmp_custom_price = order_item.custom_price
        tmp_price_per_unit = tmp_custom_price.to_f if tmp_custom_price.present?
      end

      @subtotal_amount += (order_item.quantity * tmp_price_per_unit).round(2)
      @discount_amount += (order_item.quantity * order_item.discount_amount_per_unit).round(2)
      #@tax_amount = ?
    end

    if @order.discount_percentage.nil?
      @order.discount_percentage = 0
    end

    @discount_amount = @discount_amount + (@subtotal_amount * @order.discount_percentage/100)

    @total_amount = @subtotal_amount - @discount_amount + @tax_amount

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
      #@order_items = OrderItem.where("order_id=?", @order.id).includes(item: :item_price)
      @order_items = @order.order_items.includes(item: :item_price)
    end

    def set_order_hist
      @order_hist = OrderHistory.find_by order_id: params[:id]
      @order_items_hist = @order_hist.order_item_histories
    end

    # Only allow a list of trusted parameters through.
    def order_params
      params.require(:order).permit(:name, :order_status_id, :discount_percentage)
    end

    def save_order_items
      subtotal_amount = 0
      total_discount = 0
      total_amount = 0
      count_items = 0
      #Save Order Items if present
      if params[:order_items].present?
        params[:order_items].each do |order_item|
          tmp_item_id = order_item[:item_id]

          tmp_order_item_price = ItemPrice.find_by item_id: tmp_item_id
          tmp_price_per_unit = tmp_order_item_price.price

          tmp_discount_amount_per_unit = 0
          tmp_discount_amount_per_unit = order_item[:discount_amount_per_unit].to_f if order_item[:discount_amount_per_unit].present?

          if tmp_order_item_price.variable_price
            tmp_custom_price = order_item[:custom_price]
            tmp_price_per_unit = tmp_custom_price.to_f if tmp_custom_price.present?
            OrderItem.create! order: @order, item_id: tmp_item_id, quantity: order_item[:quantity], discount_amount_per_unit: tmp_discount_amount_per_unit, custom_price: tmp_price_per_unit
          else
            OrderItem.create! order: @order, item_id: tmp_item_id, quantity: order_item[:quantity], discount_amount_per_unit: tmp_discount_amount_per_unit
          end
          
          count_items += order_item[:quantity].to_f
          subtotal_amount += (order_item[:quantity].to_f)*(tmp_price_per_unit)
          total_discount += (order_item[:quantity].to_f)*(tmp_discount_amount_per_unit)
        end
      end

      if @order.discount_percentage.nil?
        @order.discount_percentage = 0
      end

      @order.count_items = count_items
      @order.subtotal_amount = subtotal_amount.round(2)
      total_discount = total_discount + (@order.subtotal_amount * @order.discount_percentage/100)
      @order.total_discount = total_discount.round(2)
      @order.total_amount = @order.subtotal_amount - @order.total_discount
      @order.save
    end

    def init_escpos
      @cmds = "".force_encoding("ASCII-8BIT")
      write_escpos ESC + "@"
    end

    def write_escpos (data)
      @cmds << data.force_encoding("ASCII-8BIT")
    end
    #alias :<<

    def print_logo_escpos
      image = ::MiniMagick::Image.open("#{Rails.root}/public/logo_pape_kary_bn.jpg")
      print_image "G1", image
    end

    def print_line_escpos
      image = ::MiniMagick::Image.open("#{Rails.root}/public/line.jpg")
      print_image "G2", image
    end

    def print_info_escpos
      image = ::MiniMagick::Image.open("#{Rails.root}/public/info.jpg")
      print_image "G3", image
    end

    def print_image (key, image)
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr

      total_pixels = image.width * image.height
      bytes_next_m = (total_pixels)/8 + 11
      pL = bytes_next_m%256
      pH = bytes_next_m/256
      xL = image.width%256
      xH = image.width/256
      yL = image.height%256
      yH = image.height/256

      #Set graphics data: [Function 67] Define the NV graphics data (raster format)
      #128(=8*16) dots wide and 120 dots tall with respect to key code "G1"
      #        GS   ( L    pL       pH       m        fn       a      kc1/Kc2  b       xL       xH       yL       yH       c
      write_escpos GS + "(L" + pL.chr + pH.chr + 48.chr + 67.chr + 48.chr + key + 1.chr + xL.chr + xH.chr + yL.chr + yH.chr + 49.chr

      #Data image
      write_escpos data_image image

      #Set graphics data: [Function 69] Print the specified NV graphics data.
      #Prints data that corresponds to key code "G1" at 1x1 size.
      #        GS   ( L    pL      pH      m        fn     kc1/Kc2  x       y
      write_escpos GS + "(L" + 6.chr + 0.chr + 48.chr + 69.chr + key + 1.chr + 1.chr
    end

    def print_stamp_escpos
      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Select character size: Normal size
      write_escpos GS + "!" + 0.chr

      write_escpos "__________________________________________" + LF
      write_escpos "Brazas #41, Col. Villas de San Jose" + LF
      write_escpos "mail: papeleria.merceria.kary@gmail.com" + LF
      write_escpos "Facebook: /PapeleriaMerceriayRegalosKary" + LF
      write_escpos "__________________________________________" + LF
      write_escpos "" + LF

    end

    def print_date_escpos
      write_escpos " " + LF
      current_day = "FECHA: "+I18n.l(Date.today, format: :ticket, locale: :'es')
      current_hour = "HORA: "+I18n.l(DateTime.now, format: :ticket, locale: :'es')
      spaces = CHARS_LINE_ESCPOS - current_day.length - current_hour.length 

      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Print and feed paper: In case TM-T20, feeding amount = 0.250 mm (4/406 inches)
      write_escpos ESC + "J" + 4.chr
      write_escpos current_day + " "*spaces + current_hour
      #Print and feed n lines: Feed the paper three lines
      write_escpos ESC + "d" + 2.chr
    end

    def print_order_escpos
      subtotal_amount = "%.2f" % @order.subtotal_amount
      discount_percentage = "%.2f" % @order.discount_percentage
      total_discount = "%.2f" % @order.total_discount
      total_amount = "%.2f" % @order.total_amount
      paid = "%.2f" % @order.paid
      change = "%.2f" % @order.change

      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Details text data and print and line feed
      write_escpos "Cant. Descripci"+162.chr+"n                  Importe" + LF
      write_escpos "------------------------------------------" + LF

      @order_items.each do |order_item|

        tmp_order_item_price = order_item.item.item_price
        tmp_price_per_unit = tmp_order_item_price.price

        if tmp_order_item_price.variable_price
          tmp_custom_price = order_item.custom_price
          tmp_price_per_unit = tmp_custom_price.to_f if tmp_custom_price.present?
        end

        order_item_price = "%.2f" % tmp_price_per_unit
        order_item_discount = "%.2f" % order_item.discount_amount_per_unit
        order_item_subtotal = "%.2f" % (order_item.quantity * (tmp_price_per_unit - order_item.discount_amount_per_unit))
        write_escpos "#{order_item.quantity.to_s.rjust(5, " ")} #{order_item.item.name.truncate(35).ljust(35)}" + LF
        if order_item_discount.to_f == 0
          write_escpos "      #{order_item_price.ljust(7)}                      #{order_item_subtotal.rjust(7, " ")}" + LF
        else
          write_escpos "      #{order_item_price.ljust(7)} DESCUENTO #{order_item_discount.ljust(7)}    #{order_item_subtotal.rjust(7, " ")}" + LF
        end
      end

      write_escpos "------------------------------------------" + LF

      if total_discount.to_f != 0
        label_subtotal = "SUB TOTAL"
        write_escpos label_subtotal+" "*(CHARS_LINE_ESCPOS - subtotal_amount.length - label_subtotal.length) + subtotal_amount + LF

        if discount_percentage.to_f > 0
          label_promotion = "PROMOCION"
          write_escpos label_promotion+" "*(CHARS_LINE_ESCPOS - discount_percentage.length - label_promotion.length - 2) + discount_percentage + " %" + LF
        end

        label_discount = "DESCUENTOS"
        write_escpos label_discount+" "*(CHARS_LINE_ESCPOS - total_discount.length - label_discount.length) + total_discount + LF
      end

      #Select character size: horizontal (times 1) x vertical (times 2)
      write_escpos GS + "!" + 1.chr
      #Details text data and print and line feed
      label_total = "TOTAL"
      write_escpos label_total+" "*(CHARS_LINE_ESCPOS - total_amount.length - label_total.length) + total_amount + LF

      #Select character size: Normal size
      write_escpos GS + "!" + 0.chr

      #Details characters data and print and line feed
      label_paid = "PAGO"
      write_escpos label_paid+" "*(CHARS_LINE_ESCPOS - paid.length - label_paid.length) + paid + LF
      label_change = "CAMBIO"
      write_escpos label_change+" "*(CHARS_LINE_ESCPOS - change.length - label_change.length) + change + LF

    end

    def print_order_hist_escpos
      subtotal_amount = "%.2f" % @order_hist.subtotal_amount
      discount_percentage = "%.2f" % @order_hist.discount_percentage
      total_discount = "%.2f" % @order_hist.total_discount
      total_amount = "%.2f" % @order_hist.total_amount
      paid = "%.2f" % @order_hist.paid
      change = "%.2f" % @order_hist.change

      #Select justification: Left justification
      write_escpos ESC + "a" + 0.chr
      #Details text data and print and line feed
      write_escpos "Cant. Descripci"+162.chr+"n                  Importe" + LF
      write_escpos "------------------------------------------" + LF

      @order_items_hist.each do |order_item_hist|

        tmp_price_per_unit = order_item_hist.item_price

        order_item_price = "%.2f" % tmp_price_per_unit
        order_item_discount = "%.2f" % order_item_hist.discount_amount_per_unit
        order_item_subtotal = "%.2f" % (order_item_hist.quantity * (tmp_price_per_unit - order_item_hist.discount_amount_per_unit))
        write_escpos "#{order_item_hist.quantity.to_s.rjust(5, " ")} #{order_item_hist.item_name.truncate(35).ljust(35)}" + LF
        if order_item_discount.to_f == 0
          write_escpos "      #{order_item_price.ljust(7)}                      #{order_item_subtotal.rjust(7, " ")}" + LF
        else
          write_escpos "      #{order_item_price.ljust(7)} DESCUENTO #{order_item_discount.ljust(7)}    #{order_item_subtotal.rjust(7, " ")}" + LF
        end
      end

      write_escpos "------------------------------------------" + LF

      if total_discount.to_f != 0
        label_subtotal = "SUB TOTAL"
        write_escpos label_subtotal+" "*(CHARS_LINE_ESCPOS - subtotal_amount.length - label_subtotal.length) + subtotal_amount + LF

        if discount_percentage.to_f > 0
          label_promotion = "PROMOCION"
          write_escpos label_promotion+" "*(CHARS_LINE_ESCPOS - discount_percentage.length - label_promotion.length - 2) + discount_percentage + " %" + LF
        end

        label_discount = "DESCUENTOS"
        write_escpos label_discount+" "*(CHARS_LINE_ESCPOS - total_discount.length - label_discount.length) + total_discount + LF
      end

      #Select character size: horizontal (times 1) x vertical (times 2)
      write_escpos GS + "!" + 1.chr
      #Details text data and print and line feed
      label_total = "TOTAL"
      write_escpos label_total+" "*(CHARS_LINE_ESCPOS - total_amount.length - label_total.length) + total_amount + LF

      #Select character size: Normal size
      write_escpos GS + "!" + 0.chr

      #Details characters data and print and line feed
      label_paid = "PAGO"
      write_escpos label_paid+" "*(CHARS_LINE_ESCPOS - paid.length - label_paid.length) + paid + LF
      label_change = "CAMBIO"
      write_escpos label_change+" "*(CHARS_LINE_ESCPOS - change.length - label_change.length) + change + LF

    end

    def print_barcode_escpos
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      #write_escpos "<< Orden Id : #{@order.id} >>"
      #Print and feed paper: Paper feeding amount = 4.94 mm (35/180 inches)
      write_escpos ESC + "J" + 35.chr
      #Set barcode height: in case TM-T20, 6.25 mm (50/203 inches)
      write_escpos GS + "h" + 50.chr
      #Select print position of HRI characters: Print position, below the barcode
      write_escpos GS + "H" + 2.chr
      #Select font for HRI characters: Font B
      write_escpos GS + "f" + 1.chr
      #Print barcode: (A) format, barcode system = CODE39

      write_escpos GS + "k" + 4.chr + "*#{@order.id.to_s.rjust(6, "0")}*" + 0.chr
      write_escpos " " + LF
    end

    def print_barcode_hist_escpos
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      #write_escpos "<< Orden Id : #{@order.id} >>"
      #Print and feed paper: Paper feeding amount = 4.94 mm (35/180 inches)
      write_escpos ESC + "J" + 35.chr
      #Set barcode height: in case TM-T20, 6.25 mm (50/203 inches)
      write_escpos GS + "h" + 50.chr
      #Select print position of HRI characters: Print position, below the barcode
      write_escpos GS + "H" + 2.chr
      #Select font for HRI characters: Font B
      write_escpos GS + "f" + 1.chr
      #Print barcode: (A) format, barcode system = CODE39

      write_escpos GS + "k" + 4.chr + "*#{@order_hist.order_id.to_s.rjust(6, "0")}*" + 0.chr
      write_escpos " " + LF
    end

    def print_footer_escpos
      #Select justification: Centering
      write_escpos ESC + "a" + 1.chr
      #Select character size: (horizontal (times 2) x vertical (times 2))
      write_escpos GS + "!" + 1.chr
      #Print stamp data and line feed: quadruple-size character section, 1st line
      write_escpos 161.chr+"GRACIAS POR TU COMPRA" + LF
      write_escpos "VUELVE PRONTO!" + LF
      
      #Select character size: Normal size
      write_escpos GS + "!" + 0.chr
      write_escpos " " + LF
      write_escpos "Favor de revisar su mercancia y cambio" + LF
      write_escpos "No se hacen cambios ni devoluciones" + LF

    end

    def cut_escpos
      #Operating the drawer
      #Generate pulse: Drawer kick-out connector pin 2, 2 x 2 ms on, 20 x 2 ms off
      write_escpos ESC + "p" + 0.chr + 2.chr + 20.chr

      #Select cut mode and cut paper: [Function B] Feed paper to (cutting position + 0 mm) and executes a partial cut (one point left uncut).
      write_escpos GS + "V" + 66.chr + 0.chr
    end

    def send_print_escpos
      
      begin
        File.open(OUTPUT_FILE_ESCPOS,'w') do |f| 
          f << @cmds.force_encoding("UTF-8")
        end
      rescue Errno::ENOENT
        puts "There was an error writing to ESCPOS, file #{OUTPUT_FILE_ESCPOS}"
      end

    end

    def print_ticket

      #if File.file?(OUTPUT_FILE_ESCPOS)
        print_logo_escpos
        #print_line_escpos
        #print_info_escpos
        #print_line_escpos
        print_stamp_escpos
        print_date_escpos
        print_order_escpos
        print_barcode_escpos
        print_footer_escpos
        cut_escpos
        send_print_escpos
      #else
      #  puts 'No printer found'
      #end

    end

    def print_ticket_hist

      #if File.file?(OUTPUT_FILE_ESCPOS)
        print_logo_escpos
        print_stamp_escpos
        print_date_escpos
        print_order_hist_escpos
        print_barcode_hist_escpos
        print_footer_escpos
        cut_escpos
        send_print_escpos
      #else
      #  puts 'No printer found'
      #end

    end

    def data_image (mini_magic_image)
      image = mini_magic_image

      bits = []
      mask = 0x80
      i = 0
      temp = 0

      pixels = image.get_pixels

      0.upto(image.height - 1) do |y|
        0.upto(image.width - 1) do |x|
          pxR, pxG, pxB = pixels[y][x][0], pixels[y][x][1], pixels[y][x][2]
          pxValue = (pxR+pxG+pxB)/3
          value = pxValue >= 128 ? 255 : 0
          value = (value << 8) | value
          temp |= mask if value == 0
          mask = mask >> 1
          i = i + 1
          if i == 8
            bits << temp
            mask = 0x80
            i = 0
            temp = 0
          end
        end
      end

      bits.pack("C*")
    end

end
