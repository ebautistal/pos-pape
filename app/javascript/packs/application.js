// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

require("angular")
require("angular-route")

import "bootstrap"
import "../stylesheets/application"

document.addEventListener("turbolinks:load", () => {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
})

var app = angular.module('pos-pape-app', []);

//var app = angular.module('pos-pape-app', ['ngRoute']);
//app.config(['$routeProvider', function($routeProvider){
//  $routeProvider
//  .when('/:item_id/edit', {
//    controller : "NewSalesController2"
//  })
//  .otherwise({
//    controller : "NewSalesController3"
//  })
//}]);

//var INTEGER_REGEXP = /^-?\d+$/;
//app.directive('integer', function() {
//  return {
//    require: 'ngModel',
//    link: function(scope, elm, attrs, ctrl) {
//      ctrl.$validators.integer = function(modelValue, viewValue) {
//        if (ctrl.$isEmpty(modelValue)) {
//          // consider empty models to be valid
//          return true;
//        }
//
//        if (INTEGER_REGEXP.test(viewValue)) {
//          // it is valid
//          return true;
//        }
//
//        // it is invalid
//        return false;
//      };
//    }
//  };
//});

app.controller('NewSalesController', ['$scope', '$http', '$location', '$document', function($scope, $http, $location, $document) {
    var nsCtrl = this;
    
    nsCtrl.barCode = "";
    nsCtrl.enabledKeydownEvents = true;

    nsCtrl.after_action = "";
    nsCtrl.searchItemByName = "";
    nsCtrl.orderItems = [];
    nsCtrl.enableSubmit = false;
    nsCtrl.focusItemModal;

    nsCtrl.orderPercentageDiscount = angular.element(document.getElementById('order_discount_percentage')).val();
    if(!nsCtrl.orderPercentageDiscount || nsCtrl.orderPercentageDiscount<=0){
      nsCtrl.orderPercentageDiscount = "";
    }else{
      nsCtrl.orderPercentageDiscount = parseInt(nsCtrl.orderPercentageDiscount);
    }

    nsCtrl.orderName = angular.element(document.getElementById('order_name')).val();
    if(!nsCtrl.orderName){
      var today = new Date();
      var day = today.getDate();
      var month = today.getMonth()+1;
      var year = today.getFullYear();
      var hh24 = today.getHours();
      var mi = today.getMinutes();
      if(day <10) day = '0'+day;
      if(month <10) month = '0'+month;
      if(hh24 <10) hh24 = '0'+hh24;
      if(mi <10) mi = '0'+mi;

      var todayString = day+'/'+month+'/'+year+' '+hh24+':'+mi;
      nsCtrl.orderName = "Orden "+todayString;
    }

    nsCtrl.resume = {countItems:0, subtotalAmount:0, discountAmount:0, taxAmount:0, totalAmount:0};
    var resume = nsCtrl.resume;

    var init = function () {
      var absUrl = $location.absUrl();
      var path_order_id = absUrl.match(/(?<=orders\/).*?(?=\/edit)/g);
      
      if(path_order_id){
        console.log('order_id found in url: '+path_order_id[0]);
        $http.get('/orders/'+path_order_id+'/order_items.json').then(function(response) {
          var order_items = response.data;

          order_items.forEach(function(element, i) {

            var tmp_unit_price = element.price
            if(element.variable_price){
              tmp_unit_price = element.custom_price;
            }

            nsCtrl.orderItems.push( {id:element.item_id, 
              code: element.item_code, 
              name: element.item_name, 
              quantity: element.quantity, 
              unit_price: tmp_unit_price, 
              discount_amount_per_unit: element.discount_amount_per_unit,
              discount_amount_subtotal: element.quantity*element.discount_amount_per_unit,
              variable_price: element.variable_price,
              custom_price: element.custom_price
            } );
          });

          nsCtrl.calculateResume();

        });
      }

      $('#search_items').on('shown.bs.modal', function() {
        $('#item-name').focus();
      });

      // when any modal is opening
      $('.modal').on('show.bs.modal', function (e) {
        console.log("when any modal is opening");
        nsCtrl.enabledKeydownEvents = false;
      });

      // when any modal is closing
      $('.modal').on('hide.bs.modal', function (e) {
        console.log("when any modal is closing");
        nsCtrl.enabledKeydownEvents = true;
      });

      $('#btn_search_items').bind('keypress keydown keyup', function(e){
         if(e.keyCode == 13) { e.preventDefault(); }
      });

      $('#btn_save_order').bind('keypress keydown keyup', function(e){
         if(e.keyCode == 13) { e.preventDefault(); }
      });
      
    }
    
    nsCtrl.loadingSearchItems = false;

    nsCtrl.searchItems = function(){
      nsCtrl.loadingSearchItems = true;
      console.log("search items by name: "+nsCtrl.searchItemByName);
      $http.get('/items/search.json', {params: { like_search: nsCtrl.searchItemByName }})
      .then(
        function(response) {
          nsCtrl.loadingSearchItems = false;
          nsCtrl.foundItems = response.data;
        },
        function(error) {
          console.log(error);
          nsCtrl.loadingSearchItems = false;
        }
      );
    }

    nsCtrl.loadingSearchItemCode = false;

    nsCtrl.searchItemByCode = function(){
      console.log("search item by code: "+nsCtrl.barCode);
      if(nsCtrl.barCode.length == 0){
        return;
      }

      nsCtrl.loadingSearchItemCode = true;

      $http.get('/items/search.json', {params: { code: nsCtrl.barCode }})
      .then(function(response) {
        //set item found
        console.log(response.data);
        if(response.data.length==0){
          $("#search_item_code").modal("show");
          nsCtrl.searchItemCodeMessage = "No se encontro articulo con codigo: "+nsCtrl.barCode;
        }else if(response.data.length==1){
          nsCtrl.addItem(response.data[0]);
        }
        
        nsCtrl.loadingSearchItemCode = false;
        nsCtrl.barCode = "";
      });
    }

    nsCtrl.addItem = function(item){

      var existingItem = nsCtrl.orderItems.find(function(element){
        return element.id == item.id;
      });

      if(existingItem){
        existingItem.quantity = parseInt(existingItem.quantity)+1;
      }else{

        nsCtrl.orderItems.push( {id:item.id, 
          code: item.code, 
          name: item.name, 
          quantity:1, 
          unit_price: item.price, 
          discount_amount_per_unit:0,
          discount_amount_subtotal:0,
          variable_price: item.variable_price,
          custom_price: null
        } );

      }

      nsCtrl.calculateResume();
      $('#search_items').modal("hide");
    }

    nsCtrl.rmvItem = function(index){
      nsCtrl.orderItems.splice(index,1);
      nsCtrl.calculateResume();
    }

    nsCtrl.decreaseQuantity = function(index){
      var item = nsCtrl.orderItems[index];
      if(item.quantity>1) item.quantity = parseInt(item.quantity)-1;
      nsCtrl.calculateResume();
    }

    nsCtrl.increaseQuantity = function(index){
      var item = nsCtrl.orderItems[index];
      item.quantity = parseInt(item.quantity)+1;
      nsCtrl.calculateResume();
    }

    nsCtrl.showItemDetails = function(index){
      nsCtrl.focusItemModal = nsCtrl.orderItems[index];
      $("#set_quantity").modal('show');
    }

    nsCtrl.calculateResume = function(){
      nsCtrl.enableSubmit = false;
      resume.countItems = 0;
      resume.subtotalAmount = 0;
      resume.discountAmount = 0;
      resume.taxAmount = 0;
      resume.totalAmount = 0;

      nsCtrl.orderItems.forEach(function(element, i) {
        resume.countItems = resume.countItems+parseFloat(element.quantity);
        resume.subtotalAmount = resume.subtotalAmount+(element.quantity*element.unit_price);
        nsCtrl.enableSubmit = true;
        resume.discountAmount = resume.discountAmount + (element.quantity*element.discount_amount_per_unit);
        //resume.taxAmount
      });

      resume.subtotalAmount = resume.subtotalAmount.toFixed(2);
      var orderPercentageDiscount = parseFloat(nsCtrl.orderPercentageDiscount)/100;
      if(isNaN(orderPercentageDiscount)){
        orderPercentageDiscount = 0;
        nsCtrl.orderPercentageDiscount = '';
      }

      resume.discountAmount = resume.discountAmount + (parseFloat(resume.subtotalAmount)*orderPercentageDiscount);
      resume.discountAmount = resume.discountAmount.toFixed(2)

      resume.totalAmount = resume.subtotalAmount-resume.discountAmount+resume.taxAmount;
      resume.totalAmount = resume.totalAmount.toFixed(2);

    }

    nsCtrl.submitOrder = function(){
      $("#after_action").val("submit_order");
      $("#form_order").submit();
    }

    init();

    $scope.$on('pospape-keydownevent', function (event, args) {
      if(!nsCtrl.enabledKeydownEvents){
        return;
      }

      var keycode = args.keyCode;
      if(keycode == 13){
        console.log("perform some action with string: "+nsCtrl.barCode);
        nsCtrl.searchItemByCode();
      }else if(keycode == 27){
        nsCtrl.barCode = "";
      }else if( 
          (keycode > 47 && keycode < 58)   || // number keys
          //keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
          (keycode > 64 && keycode < 91)   || // letter keys
          (keycode > 95 && keycode < 112)  || // numpad keys
          (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
          (keycode > 218 && keycode < 223)   // [\]' (in order)
        ){
        nsCtrl.barCode = nsCtrl.barCode + args.key;
      }

      $scope.$apply();
    });

}]);

app.controller('SubmitOrderController', ['$scope', '$http', function($scope, $http) {

    var nsCtrl = this;

    nsCtrl.paid = "";
    nsCtrl.change = 0;
    nsCtrl.order_resume = [];

    var order_id = $("#order_id").val();

    var init = function () {
      
      if(order_id){
        console.log('order_id found: '+order_id);
        $http.get('/orders/'+order_id+'/resume.json').then(function(response) {
          nsCtrl.order_resume = response.data;
        });
      }
      
    }

    init();

}]);

app.controller('PrintOrderController', ['$scope', '$http', function($scope, $http) {

    var poCtrl = this;

    poCtrl.printOrder = function(orderId){
      console.log('printing orderId: '+orderId);
      $http.get('/orders/'+orderId+'/print_order').then(function(response) {
        console.log(response);
      });
    }

}]);

app.controller('NavBarController', ['$scope', '$http', function($scope, $http) {

    var nsCtrl = this;

    nsCtrl.printInfoPape = function(){
        $http.get('/orders/print_info_pape');
    }

}]);
// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)


app.directive('keydownEvents', ['$document', function ($document) {
    return {
        restrict: 'A',
        link: function (scope) {
            $document.bind('keydown', function (e) {
                if(e.target.tagName === 'INPUT'){
                  return;
                }

                console.log(e.key+" "+e.keyCode);
                scope.$broadcast('pospape-keydownevent', { key: e.key, keyCode: e.keyCode });
            });
        }
    }
}]);
