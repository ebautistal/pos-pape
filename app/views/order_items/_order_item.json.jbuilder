json.extract! order_item, :id, :order_id, :item_id, :quantity, :created_at, :updated_at
json.url order_order_item_url(order_item.order_id, order_item.id, format: :json)
