#json.array! @order_items, partial: "order_items/order_item", as: :order_item

json.array! @order_items do |order_item|
  json.order_id order_item.order_id
  json.order_name order_item.order.name
  json.item_id order_item.item.id
  json.item_code order_item.item.code
  json.item_name order_item.item.name
  json.quantity order_item.quantity
  json.price order_item.item.item_price.price
  if order_item.discount_amount_per_unit.present?
    json.discount_amount_per_unit order_item.discount_amount_per_unit
  else
    json.discount_amount_per_unit 0
  end
  json.variable_price order_item.item.item_price.variable_price
  json.custom_price order_item.custom_price
  
  #json.extract! order_item, :order_id, order_item.order.name, order_item.item.id, order_item.item.name, order_item.quantity, order_item.item.item_price.price
  #json.brand do
  #	json.id item.brand_id
  #	json.name item.brand.name
  #	json.description item.brand.description
  #end
  #json.price item.item_price.price
end