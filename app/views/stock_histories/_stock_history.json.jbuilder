json.extract! stock_history, :id, :item_id, :units, :buy_price, :created_at, :updated_at
json.url stock_history_url(stock_history, format: :json)
