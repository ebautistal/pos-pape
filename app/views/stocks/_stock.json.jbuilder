json.extract! stock, :id, :item_id, :units, :buy_price, :created_at, :updated_at
json.url stock_url(stock, format: :json)
