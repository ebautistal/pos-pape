json.extract! order, :id, :name, :order_status_id, :created_at, :updated_at
json.url order_url(order, format: :json)
