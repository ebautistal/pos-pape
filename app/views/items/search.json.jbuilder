#json.array! @brands do |brand|
#  json.name brand.name
#  json.description brand.description
#  json.items brand.item do |item|
#    json.extract! item, :name, :description, :brand, :code
#  end
#end


json.array! @items do |item|
  json.extract! item, :id, :name, :description, :code
  json.brand do
  	json.id item.brand_id
  	json.name item.brand.name
  	json.description item.brand.description
  end
  json.price item.item_price.price
  json.variable_price item.item_price.variable_price
  if item.stock.present?
    if item.stock.shared_stock 
      if item.stock.shared_stock_item_id.present?
        stockItemShared = Stock.find_by(item_id: item.stock.shared_stock_item_id)
        json.stock_units (stockItemShared.units / item.stock.shared_stock_consume_units)
      else
        json.stock_units "error"
      end
    else
      json.stock_units item.stock.units
    end
  else
  	json.stock_units nil	
  end
end