class ItemPrice < ApplicationRecord
  belongs_to :item

  before_validation :default_values
 
  protected
  def default_values
    
    if variable_price.blank?
    	self.variable_price = false
    end
    
  end
end
