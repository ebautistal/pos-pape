class Item < ApplicationRecord
  belongs_to :brand
  belongs_to :item_type
  has_one :item_price, :dependent => :destroy
  has_one :stock, :dependent => :destroy

  validates_presence_of :name
  validates :name, uniqueness: true
  validates :code, uniqueness: true, allow_blank: true

  before_validation :default_values
  #before_destroy :drop_related

  protected
  def default_values
    
    if code.blank?
    	self.code = nil
    end
    
    if brand_id.nil?
      self.brand_id = Brand.find_by(name: "GENERICO").id
    end

    if item_type_id.nil?
      self.item_type_id = ItemType.find_by(name: "ITEM").id
    end

  end

#  def drop_related
#      Stock.where("item_id=?", id).delete_all
#      ItemPrice.where("item_id=?", id).delete_all
#    end

end
