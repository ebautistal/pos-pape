class Order < ApplicationRecord
  belongs_to :order_status
  has_many :order_items

  before_destroy :create_history

  private
  	def create_history
  	  order_history = OrderHistory.create!(order_id: id, name: name, order_status: order_status, count_items: count_items, subtotal_amount: subtotal_amount, total_discount: total_discount, total_amount: total_amount, paid: paid, change: change, discount_percentage: discount_percentage)

  	  order_items = OrderItem.where("order_id=?", id).includes(item: :item_price)

  	  order_items.each do |order_item|
        tmp_order_item_price = order_item.item.item_price
        tmp_price_per_unit = tmp_order_item_price.price

        if tmp_order_item_price.variable_price
          tmp_custom_price = order_item.custom_price
          tmp_price_per_unit = tmp_custom_price.to_f if tmp_custom_price.present?
        end

        OrderItemHistory.create! order_history_id: order_history.id, order_id: order_item.order_id, item_id: order_item.item_id, quantity: order_item.quantity, discount_amount_per_unit: order_item.discount_amount_per_unit, item_name: order_item.item.name, item_price: tmp_price_per_unit

        stockItem = order_item.item.stock
        if stockItem.blank?
          stockItem = Stock.create! item_id: order_item.item_id
        end

        if !stockItem.shared_stock
          stockItem.units = 0 if stockItem.units.blank?
          stockItem.units -= order_item.quantity
          stockItem.save
        else
          if stockItem.shared_stock_item_id.present?
            stockItemShared = Stock.find_by(item_id: stockItem.shared_stock_item_id)
            stockItemShared.units = 0 if stockItemShared.units.blank?
            stockItemShared.units -= (order_item.quantity*stockItem.shared_stock_consume_units)
            stockItemShared.save
          else
            puts "There is an error in configuration of shared item, please check"
            stockItem.units = 0 if stockItem.units.blank?
            stockItem.units -= order_item.quantity
            stockItem.save
          end
        end

      end
      OrderItem.where("order_id=?", id).delete_all

  	end
end
