class OrderHistory < ApplicationRecord
  belongs_to :order_status
  has_many :order_item_histories

end
