class Stock < ApplicationRecord
  belongs_to :item

  before_validation :default_values
 
  protected
  def default_values
    
    if item_shareable.blank?
    	self.item_shareable = false
    end
    
    if shared_stock.blank?
    	self.shared_stock = false
    end

  end
end
