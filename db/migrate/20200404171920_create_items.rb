class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :code, index: { unique: true }
      t.references :brand, null: false, foreign_key: true
      t.references :item_type, null: false, foreign_key: true
      t.string :name, null: false
      t.text :description

      t.timestamps
    end

    add_index :items, [:brand_id, :item_type_id, :name], unique: true
  end
end
