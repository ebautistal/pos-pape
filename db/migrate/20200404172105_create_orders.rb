class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :name
      t.references :order_status, null: false, foreign_key: true
      t.float :count_items
      t.float :subtotal_amount
      t.float :total_discount
      t.float :total_amount
      t.float :paid
      t.float :change

      t.timestamps
    end
  end
end
