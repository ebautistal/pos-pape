class AddSharedStockToStocks < ActiveRecord::Migration[6.0]
  def change
  	add_column :stocks, :item_shareable, :boolean, :default => false
    add_column :stocks, :shared_stock, :boolean, :default => false
    add_reference :stocks, :shared_stock_item, null: true, foreign_key: { to_table: 'items' }
    add_column :stocks, :shared_stock_consume_units, :float
  end
end
