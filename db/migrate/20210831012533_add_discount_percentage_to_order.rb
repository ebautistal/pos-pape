class AddDiscountPercentageToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :discount_percentage, :float, :default => 0
    add_column :order_histories, :discount_percentage, :float, :default => 0
  end
end
