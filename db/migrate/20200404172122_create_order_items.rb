class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_table :order_items do |t|
      t.references :order, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: true
      t.float :quantity
      t.float :discount_amount_per_unit
      t.float :custom_price

      t.timestamps
    end
  end
end
