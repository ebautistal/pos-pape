class CreateStockHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :stock_histories do |t|
      t.references :item, null: false, foreign_key: true
      t.float :units
      t.float :buy_price

      t.timestamps
    end
  end
end
