class CreateOrderItemHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :order_item_histories do |t|
      t.references :order_history, null: false, foreign_key: true
      t.integer :order_id
      t.integer :item_id, null: false
      t.float :quantity
      t.float :discount_amount_per_unit
      t.string :item_name
      t.float :item_price

      t.timestamps
    end
  end
end
