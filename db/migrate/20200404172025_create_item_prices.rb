class CreateItemPrices < ActiveRecord::Migration[6.0]
  def change
    create_table :item_prices do |t|
      t.references :item, null: false, foreign_key: true
      t.float :price
      t.boolean :variable_price 

      t.timestamps
    end
  end
end
