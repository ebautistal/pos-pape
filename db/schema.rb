# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_31_012533) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_brands_on_name", unique: true
  end

  create_table "item_prices", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.float "price"
    t.boolean "variable_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_item_prices_on_item_id"
  end

  create_table "item_types", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_item_types_on_name", unique: true
  end

  create_table "items", force: :cascade do |t|
    t.string "code"
    t.bigint "brand_id", null: false
    t.bigint "item_type_id", null: false
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["brand_id", "item_type_id", "name"], name: "index_items_on_brand_id_and_item_type_id_and_name", unique: true
    t.index ["brand_id"], name: "index_items_on_brand_id"
    t.index ["code"], name: "index_items_on_code", unique: true
    t.index ["item_type_id"], name: "index_items_on_item_type_id"
  end

  create_table "order_histories", force: :cascade do |t|
    t.integer "order_id"
    t.string "name"
    t.bigint "order_status_id", null: false
    t.float "count_items"
    t.float "subtotal_amount"
    t.float "total_discount"
    t.float "total_amount"
    t.float "paid"
    t.float "change"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "discount_percentage", default: 0.0
    t.index ["order_status_id"], name: "index_order_histories_on_order_status_id"
  end

  create_table "order_item_histories", force: :cascade do |t|
    t.bigint "order_history_id", null: false
    t.integer "order_id"
    t.integer "item_id", null: false
    t.float "quantity"
    t.float "discount_amount_per_unit"
    t.string "item_name"
    t.float "item_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_history_id"], name: "index_order_item_histories_on_order_history_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.bigint "item_id", null: false
    t.float "quantity"
    t.float "discount_amount_per_unit"
    t.float "custom_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_order_items_on_item_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string "name"
    t.bigint "order_status_id", null: false
    t.float "count_items"
    t.float "subtotal_amount"
    t.float "total_discount"
    t.float "total_amount"
    t.float "paid"
    t.float "change"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "discount_percentage", default: 0.0
    t.index ["order_status_id"], name: "index_orders_on_order_status_id"
  end

  create_table "stock_histories", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.float "units"
    t.float "buy_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["item_id"], name: "index_stock_histories_on_item_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.bigint "item_id", null: false
    t.float "units"
    t.float "buy_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "item_shareable", default: false
    t.boolean "shared_stock", default: false
    t.bigint "shared_stock_item_id"
    t.float "shared_stock_consume_units"
    t.index ["item_id"], name: "index_stocks_on_item_id"
    t.index ["shared_stock_item_id"], name: "index_stocks_on_shared_stock_item_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "item_prices", "items"
  add_foreign_key "items", "brands"
  add_foreign_key "items", "item_types"
  add_foreign_key "order_histories", "order_statuses"
  add_foreign_key "order_item_histories", "order_histories"
  add_foreign_key "order_items", "items"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "order_statuses"
  add_foreign_key "stock_histories", "items"
  add_foreign_key "stocks", "items"
  add_foreign_key "stocks", "items", column: "shared_stock_item_id"
end
